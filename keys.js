// This function takes an object as input and returns an array containing its keys
function keys(obj) {
  // Create an empty array to store the keys
  let result = [];

  // Iterate through each property of the object
  for (key in obj) {
    // Push the current key into the result array
    result.push(key);
  }

  // Return the array containing the keys
  return result;
}

// Export the keys function to make it accessible in other files
module.exports = keys;
