// Function to take an object as input and returns an array of key-value pairs from the object.
function pairs(obj) {
  // Initialize an empty array to store key-value pairs
  let result = [];

  // Iterate over each property (key) in the input object
  for (let key in obj) {
    // Push an array containing the key and its corresponding value to the result array
    result.push([key, obj[key]]);
  }

  // Return the array of key-value pairs
  return result;
}

// Export the pairs function to make it available for use in other files
module.exports = pairs;
