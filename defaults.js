/*Function takes an object and a set of default properties as input,
 and adds the default properties to the object if they are undefined */
function defaults(obj, defaultProps) {
  // Flag to check if a default property already exists in the object
  let flag = 0;

  // Iterate over each default property in the defaultProps object
  for (let defaultKey in defaultProps) {
    // Reset the flag for each default property
    flag = 0;

    // Iterate over each property in the input object
    for (let key in obj) {
      // Check if the current default property already exists in the object
      if (defaultKey === key) {
        // Set the flag to 1 and break out of the inner loop
        flag = 1;
        break;
      }
    }

    /*If the flag is 0, the default property does not exist in the object,
    so add it with the corresponding default value */
    if (flag === 0) {
      obj[defaultKey] = defaultProps[defaultKey];
    }
  }

  // Return the modified object with added default properties
  return obj;
}

// Export the defaults function to make it available for use in other files
module.exports = defaults;
