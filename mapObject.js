/* This function takes an object and a callback function as input,
and returns an array containing the values transformed by the callback.*/

function mapObject(obj, cb) {
  // Create an empty array to store the transformed values.
  let result = [];

  // Iterate through each property of the object.
  for (key in obj) {
    // Apply the callback function to the current property value and push the result into the array.
    result.push(cb(obj[key]));
  }

  // Return the array containing the transformed values.
  return result;
}

// Export the mapObject function to make it accessible in other files.
module.exports = mapObject;
