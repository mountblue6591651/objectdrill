//Function to take an object as input and returns a new object with keys and values swapped.
function invert(obj) {
  // Initialize an empty object to store the inverted key-value pairs
  let result = {};

  // Iterate over each key in the input object
  for (let key in obj) {
    // Assign the current key's value as a new key and the current key as its value
    result[obj[key]] = key;
  }

  // Return the object with inverted key-value pairs
  return result;
}

// Export the invert function to make it available for use in other files
module.exports = invert;
