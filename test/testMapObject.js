// Import the mapObject function from the mapObject.js file.
const mapObject = require("../mapObject.js");

// Create an object for testing the mapObject function.
const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

// Call the mapObject function to transform values based on the provided mapping function.
let result = mapObject(testObject, (value) => {
  // Check if the value is a string.
  if (typeof value === "string") {
    // If it's a string, convert it to uppercase.
    return value.toUpperCase();
  } else if (typeof value === "number") {
    // If it's a number, double its value.
    return value * 2;
  } else {
    // If it's neither a string nor a number, return the original value.
    return value;
  }
});

// Display the result (transformed object) in the console.
console.log(result);
