// Import the 'defaults' function from '../defaults.js'
const defaults = require("../defaults.js");

// Create a test object
const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

// Call the 'defaults' function with the test object and a set of default properties
// The function adds default properties to the object if they do not already exist
console.log(defaults(testObject, { alias: "Batman" }));

// Log the modified object to the console
