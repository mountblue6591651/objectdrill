// Import the values function from the values.js file
const values = require("../values.js");

// Create an object for testing the values function
const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

// Call the values function to retrieve the values of the test object.
let result = values(testObject);

// Display the result (array of values) in the console.
console.log(result);
