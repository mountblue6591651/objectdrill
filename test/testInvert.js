// Import the 'invert' function from '../invert.js'
const invert = require("../invert.js");

// Create a test object 
const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

// Call the 'invert' function with the test object to swap keys and values
let result = invert(testObject);

// Log the result (inverted object) to the console
console.log(result);
