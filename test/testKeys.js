// Import the keys function from the keys.js file
const keys = require("../keys.js");

// Create an object for testing the keys function.
const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

// Call the keys function to retrieve the keys of the test object.
let result = keys(testObject);

// Display the result (array of keys) in the console.
console.log(result);
