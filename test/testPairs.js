// Import the 'pairs' function from '../pairs.js'
const pairs = require("../pairs.js");

// Create a test object with properties like name, age, and location
const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

// Call the 'pairs' function with the test object to generate pairs
let result = pairs(testObject);

// Log the result (pairs) to the console
console.log(result);
