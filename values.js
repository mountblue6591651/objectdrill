// This function takes an object as input and returns an array containing its non-function values.
function values(obj) {

    // Create an empty array to store the values.
  let result = [];

  // Iterate through each property of the object.
  for (value in obj) {
    // Check if the current property value is a function.
    if (typeof obj[value] === "function") {
      // If it's a function, skip to the next iteration.
      continue;
    } else {
      // If it's not a function, push the value into the result array.
      result.push(obj[value]);
    }
  }

  // Return the array containing the non-function values.
  return result;
}

// Export the values function to make it accessible in other modules.
module.exports = values;
